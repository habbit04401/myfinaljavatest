package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int num , x[][] = new int [5][100];
        Scanner scanner = new Scanner(System.in);
        num = scanner.nextInt();
        for(int j = 0;j < num ; j++){
            for(int i = 0;i<5;i++){
                x[i][j]=(int)(Math.random()*101)+1;
                System.out.print(x[i][j]+" ");
            }
            System.out.println();
        }
        int A = 0,B=0,C=0,D=0,E=0;
        for(int j=0;j<num;j++){
            for(int i = 0;i<5;i++){
                if (x[i][j]>=90)
                    A++ ;
                if (x[i][j]<=89 && x[i][j] >=80)
                    B++ ;
                if (x[i][j]<=79 && x[i][j] >=70)
                    C++ ;
                if (x[i][j]<=69 && x[i][j] >=60)
                    D++ ;
                if (x[i][j]<60)
                    E++ ;
            }
        }
        System.out.println(">=90: "+A);
        System.out.println("80-89: "+B);
        System.out.println("70-79: "+C);
        System.out.println("60-69: "+D);
        System.out.println("<60: "+E);
    }
}
